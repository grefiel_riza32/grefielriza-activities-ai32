<?php

use Illuminate\Http\Request;


use App\Http\Controllers\Api\PlaylistController as ApiPlaylistController;
use App\Http\Controllers\Api\PlaylistSongController as ApiPlaylistSongController;
use App\Http\Controllers\Api\SongController as ApiSongController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'song' => ApiSongController::class,
    'playlist' => ApiPlaylistController::class,
    'playlistsong' => ApiPlaylistSongController::class

]);
